// 1. В JS існують такі типи даних: примітивні - рядок string, число number, булевий чи логічний boolean, велике число bigint, symbol, null та undefined;
// та особливі типи - об'єкти та функції.

// 2. == i === це оператори порівняння, але === строге порівняння, буде шукати стовідсткове співпадіння і по значенню, і по типу даних. 
// а == допускає приведення одного типу до іншого і як наслідок схожі значення, наприклад null може перетворюватися на 0,
// а parseInt від "10рх" візьме 10 і буде 10, але тільки при ==, тобто не строгому порівнянні спрацює.

// 3. Оператор, це інструкція що саме ми повинні зробити з чимось (з операндами). 
// Інсують арифметичні оператори, оператори порівняння, оператори присвоєння, логічні оператори, умовні, рядкові та інші.


let nameUser = prompt("Please, enter your name.");
console.log(`Your name is ${(nameUser)}`);

while (true) {
    // if (nameUser == 0 || nameUser == "" || nameUser == undefined || nameUser == null) - всі перевірки, що я пробувала застосовувати
    if (nameUser === "" || !nameUser || nameUser.trim() === "") {
        nameUser = prompt("Please, enter your name again.");
        console.log(`Your name is ${(nameUser)}`);
    } 
    else {
        console.log(`Your name is ${(nameUser)}`);
        break;
    }
}


let ageUser = prompt("Please, enter your age.");
console.log(`Your age is ${(ageUser)}`);

while (true) {
    if (ageUser === "" || !ageUser || isNaN(ageUser) || ageUser.trim() === ""){
        ageUser = prompt("Please, enter your age again.");
        console.log(`Your age is ${(ageUser)}`);
    }
    else {
        console.log(`Your age is ${(ageUser)} `);
        break;
    }
}

if (ageUser < 18) {
    alert("You are not allowed to visit this website");
}
else if (ageUser >= 18 && ageUser <= 22) {
    let answer;
    answer = confirm("Are you sure you want to continue?");
    if (answer == false) {
        alert("You are not allowed to visit this website");
    }
    else {
        alert(`Welcome, ${ (nameUser) } `);
    }
}
else {
    alert(`Welcome, ${ (nameUser) } `);
}
